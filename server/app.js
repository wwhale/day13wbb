


var Sequelize = require("sequelize");

console.log(Sequelize);

const SQL_USERNAME="root"
const SQL_PASSWORD="1111"

var connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3308,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max : 5,
            min : 0,
            idle : 10000
        }
    }
);

connection.query("SELECT * from employees LIMIT 10", 
{type: connection.QueryTypes.SELECT})
.then(users =>{
 console.log(users);

});

connection.query("SELECT * from employees where last_name = :last_name LIMIT 10", 
{ replacements: {last_name: 'Erde'}},
    {type: connection.QueryTypes.SELECT})
    .then(users =>{
     console.log(users);

    });


    connection.query("SELECT * from employees where last_name like :last_name LIMIT 10", 
    { replacements: {last_name: '%er%'}},
        {type: connection.QueryTypes.SELECT})
        .then(users =>{
         console.log(users);
    
        });